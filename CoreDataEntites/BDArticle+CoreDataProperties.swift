//
//  BDArticle+CoreDataProperties.swift
//  NewsApp
//
//  Created by Elena Ashueva
//
//

import Foundation
import CoreData


extension BDArticle {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BDArticle> {
        return NSFetchRequest<BDArticle>(entityName: "BDArticle")
    }

    @NSManaged public var id: String?
    @NSManaged public var typeSelect: Int32
    @NSManaged public var date: Date?
}
