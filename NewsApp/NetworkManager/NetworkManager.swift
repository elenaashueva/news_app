//
//  NetworkManager.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation

class NetworkManager {

    private init() {}

    static let shared = NetworkManager()
    
    func getNews(url: String, result: @escaping ((OfferModel?) -> ())) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "newsapi.org"
        urlComponents.path = "/v2/everything"
        urlComponents.queryItems = [URLQueryItem(name: "q", value: "bitcoin"),
                                    URLQueryItem(name: "apikey", value: "a4e8a8c0d4c0406f9ac5572043272df7")]
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let task = URLSession(configuration: .default)
        task.dataTask(with: request) { (data, response, error) in
            if error == nil {
                let decoder = JSONDecoder()
                var decoderOfferModel: OfferModel?
                
                if data != nil {
                    decoderOfferModel = try? decoder.decode(OfferModel.self, from: data!)
                }
                
                result(decoderOfferModel)
            } else {
                print(error as Any)
            }
        }.resume()
    }
}


// https://newsapi.org/v2/everything?q=bitcoin&apiKey=a4e8a8c0d4c0406f9ac5572043272df7h
