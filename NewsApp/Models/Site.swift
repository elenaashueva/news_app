//
//  Site.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation

class Site {
    let name: String
    let url: String
    
    init(url: String, name: String) {
        self.name = name
        self.url = url
    }
}
