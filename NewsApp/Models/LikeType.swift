//
//  LikeType.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation

enum LikeType: Int {
    case like
    case dislike
    case none
}
