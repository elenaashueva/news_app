//
//  OfferModel.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation

class OfferModel: Codable {
    var status: String?
    var totalResults: Int?
    var articles: [ArticleModel]?
}
