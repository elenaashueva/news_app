//
//  ArticleModel.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation

class ArticleModel: Codable {
    var title: String?
    var description: String?
    var urlToImage: String?
    var publishedAt: String?
    var source: Source?
}
