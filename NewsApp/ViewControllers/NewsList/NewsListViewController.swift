//
//  NewsList.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation
import UIKit

class NewsListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var currentSite: Site?
    var dataIsReady: Bool = false
    var offerModel: OfferModel! {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

// MARK: - Life Cycle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        loadNews()
        configureNavigationBar()
    }
}

// MARK: - Configure
extension NewsListViewController {

    func configureNavigationBar() {
        navigationItem.title = currentSite?.name
    }
    
    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.register(UINib(nibName: "NewsListTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "NewsListTableViewCell")
    }

    func loadNews() {
        NetworkManager.shared.getNews(url: "url") { (offerModel) in
            DispatchQueue.main.async { [weak self] in
                self?.dataIsReady = true
                self?.offerModel = offerModel
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension NewsListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataIsReady {
            return self.offerModel!.articles!.count
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsListTableViewCell") as! NewsListTableViewCell
        cell.iconImageView.loadImage(withUrl: offerModel.articles![indexPath.row].urlToImage ?? "")
        cell.titleLabel.text = offerModel.articles![indexPath.row].title
        cell.descriptionLabel.text = offerModel.articles![indexPath.row].description
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension NewsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let articles = offerModel.articles else { return }
        openNewsDescription(article: articles[indexPath.row])
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

// MARK: - Open ViewController
extension NewsListViewController {
    
    func openNewsDescription (article: ArticleModel) {
        guard let viewController = UIStoryboard(name: "NewsDescription",
                                                bundle: nil).instantiateInitialViewController() as? NewsDescriptionViewController else { fatalError("Can't load view controller from storyboard") }
        viewController.article = article
        let navigationController = UINavigationController(rootViewController: viewController)
        self.navigationController?.present(navigationController, animated: true)
    }
}

