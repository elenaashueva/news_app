//
//  NewsListTableViewCell.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import UIKit

class NewsListTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureImageView()
    }
}

// MARK: - Configure
extension NewsListTableViewCell {

    func configureImageView() {
        iconImageView.layer.cornerRadius = 16
        iconImageView.clipsToBounds = true
    }
}
