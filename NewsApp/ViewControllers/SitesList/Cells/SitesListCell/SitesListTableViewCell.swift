//
//  SitesListTableViewCell.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import UIKit

class SitesListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var urlLabel: UILabel!
}
