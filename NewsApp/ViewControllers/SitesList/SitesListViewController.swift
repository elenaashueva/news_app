//
//  ViewController.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation
import UIKit
import CoreData

class SitesListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var aboutAuthorButton: UIButton!
    let site = Site(url: "https://newsapi.org/v2/everything?q=bitcoin", name: "newsapi.org")

    var sites: [Site] = []

// MARK: - Life Cycle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        sites.append(site)
        configureTableView()
        configureAboutAuthor()
        configureNavigationBar()
    }
    

// MARK: - Actions
    @IBAction func showAboutAuthor(_ sender: Any) {
        openAboutAuthorViewController()
    }
}

// MARK: - Configure
extension SitesListViewController {
    
    func configureNavigationBar() {
        navigationItem.title = "Новости"
    }

    func configureAboutAuthor() {
        aboutAuthorButton.layer.cornerRadius = aboutAuthorButton.frame.height / 2
    }

    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "SitesListTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "SitesListTableViewCell")
    }
}

// MARK: - UITableViewDataSource
extension SitesListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sites.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SitesListTableViewCell", for: indexPath)
        
        let site = sites[indexPath.row]

        if let myCell = cell as? SitesListTableViewCell {
            myCell.nameLabel.text = site.name
            myCell.nameLabel.numberOfLines = 0
            myCell.nameLabel.font = UIFont.boldSystemFont(ofSize: 24)
            myCell.nameLabel.textColor = .black
            myCell.urlLabel.text = site.url
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SitesListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print(sites[indexPath.row])
        
        let site = sites[indexPath.row]
        openNewListViewController(site: site)
    }
}

// MARK: - Open ViewController
extension SitesListViewController {

    func openNewListViewController (site: Site) {
        guard let viewController = UIStoryboard(name: "NewsList",
                                                bundle: nil).instantiateInitialViewController() as? NewsListViewController else { fatalError("Can't load view controller from storyboard") }
        viewController.currentSite = site
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    func openAboutAuthorViewController() {
        guard let viewController = UIStoryboard(name: "AboutAuthor",
                                                bundle: nil).instantiateInitialViewController() as? AboutAuthorViewController else { fatalError("Can't load view controller from storyboard") }
        navigationController?.pushViewController(viewController, animated: true)
    }
}

// a4e8a8c0d4c0406f9ac5572043272df7
