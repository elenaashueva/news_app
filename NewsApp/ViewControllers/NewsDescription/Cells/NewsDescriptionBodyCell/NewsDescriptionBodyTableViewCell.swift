//
//  NewsDescriptionBodyTableViewCell.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import UIKit

class NewsDescriptionBodyTableViewCell: UITableViewCell {

    @IBOutlet weak var bodyLabel: UILabel!
}
