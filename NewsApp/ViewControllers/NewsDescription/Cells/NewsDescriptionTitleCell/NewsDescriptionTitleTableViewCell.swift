//
//  NewsDescriptionTitleTableViewCell.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import UIKit

class NewsDescriptionTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
}
