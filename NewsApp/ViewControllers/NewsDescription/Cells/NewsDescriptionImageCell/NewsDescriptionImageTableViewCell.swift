//
//  NewsDescriptionImageTableViewCell.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import UIKit

class NewsDescriptionImageTableViewCell: UITableViewCell {

    @IBOutlet weak var avaImageView: UIImageView!
}
