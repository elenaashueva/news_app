//
//  DescriptionNews.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation
import UIKit
import CoreData

class NewsDescriptionViewController: UIViewController {
    
    var context: NSManagedObjectContext!
    var bdArticle: BDArticle?
    var article: ArticleModel?
    var typeLike: LikeType = .none

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dislikeButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    
// MARK: - Life Cycle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getArticlesFromBD()
        guard let articleID = bdArticle?.typeSelect, let type = LikeType(rawValue: Int(articleID)) else { return }
        typeLike = type
        configureButtons()
    }
    
// MARK: - Actions
    @objc func closeTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func likeAction(_ sender: UIButton) {
        typeLike = .like
        configureButtons()
        saveBDArticle(typeLike: .like)
    }
    
    @IBAction func dislikeAction(_ sender: UIButton) {
        typeLike = .dislike
        configureButtons()
        saveBDArticle(typeLike: .dislike)
    }
}

// MARK: - CoreData
extension NewsDescriptionViewController {
    
    func getBDArticles() -> BDArticle? {
        let context = CoreDataStack.persistentContainer.viewContext
        guard let bdArticleID = bdArticle?.id else {
            return nil
        }
        let fetchRequest: NSFetchRequest<BDArticle> = BDArticle.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = %@", bdArticleID)
        do {
            return try context.fetch(fetchRequest).first
        } catch let error as NSError {
            print("Error: \(error), userInfo: \(error.userInfo)")
        }
        return nil
    }
    
    func getIdOrTitleArticles() -> String? {
        if let articleID = article?.source?.id {
            return articleID
        } else if let titleArticle = article?.title {
            return titleArticle
        }
        return nil
    }
    
    func getArticlesFromBD() {
        let context = CoreDataStack.persistentContainer.viewContext
        guard let id = getIdOrTitleArticles() else { return }
        let fetchRequest: NSFetchRequest<BDArticle> = BDArticle.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = %@", id)
        do {
            bdArticle = try context.fetch(fetchRequest).first
        } catch let error as NSError {
            print("Error: \(error), userInfo: \(error.userInfo)")
        }
    }
    
    func saveBDArticle (typeLike: LikeType) {
        let context = CoreDataStack.persistentContainer.viewContext
        if let bdArticle = getBDArticles() {
            bdArticle.typeSelect = Int32(typeLike.rawValue)
        } else {
            guard let id = getIdOrTitleArticles() else { return }
            let entity = NSEntityDescription.entity(forEntityName: "BDArticle", in: context)
            let bdArticleObject = NSManagedObject(entity: entity!, insertInto: context) as! BDArticle
            bdArticleObject.date = Date()
            bdArticleObject.typeSelect = Int32(typeLike.rawValue)
            bdArticleObject.id = id
        }
        do {
            try context.save()
        } catch let error as NSError {
            print("Error: \(error), userInfo: \(error.userInfo)")
        }
        tableView.reloadData()
    }
}

// MARK: - Configure
extension NewsDescriptionViewController {
    
    func configureNavigationBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close,
                                                            target: self,
                                                            action: #selector(closeTapped))
    }
    
    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "NewsDescriptionImageTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "NewsDescriptionImageTableViewCell")
        
        self.tableView.register(UINib(nibName: "NewsDescriptionTitleTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "NewsDescriptionTitleTableViewCell")
        
        self.tableView.register(UINib(nibName: "NewsDescriptionBodyTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "NewsDescriptionBodyTableViewCell")
    }
    
    func configureButtons() {
        switch typeLike {
        case .none:
            likeButton.isEnabled = true
            likeButton.alpha = 1
            dislikeButton.isEnabled = true
            dislikeButton.alpha = 1
        case .like:
            likeButton.isEnabled = false
            likeButton.alpha = 0.4
            dislikeButton.isEnabled = true
            dislikeButton.alpha = 1
        case .dislike:
            likeButton.isEnabled = true
            likeButton.alpha = 1
            dislikeButton.isEnabled = false
            dislikeButton.alpha = 0.4
        }
    }
}

// MARK: - UITableViewDelegate
extension NewsDescriptionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch  indexPath.row {
        case 0:
            return 200
        case 1:
            return 50
        case 2:
            return 150
        case 3:
            return 50
        default:
            return 0
        }
    }
}

// MARK: - UITableViewDataSource
extension NewsDescriptionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDescriptionImageTableViewCell") as! NewsDescriptionImageTableViewCell
            cell.avaImageView.loadImage(withUrl: article!.urlToImage ?? "")
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDescriptionTitleTableViewCell") as! NewsDescriptionTitleTableViewCell
            cell.titleLabel.text = article?.title
            cell.titleLabel.numberOfLines = 0
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDescriptionBodyTableViewCell") as! NewsDescriptionBodyTableViewCell
            cell.bodyLabel.text = article?.description
            cell.bodyLabel.numberOfLines = 0
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsDescriptionBodyTableViewCell") as! NewsDescriptionBodyTableViewCell
            cell.bodyLabel.text = article?.publishedAt
            cell.bodyLabel.numberOfLines = 0
            cell.bodyLabel.textAlignment = .center
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
}

