//
//  SitesList.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation
import UIKit

class AboutAuthorViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var titleAboutAuthor = ""
    
// MARK: - Life Cycle ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
}

// MARK: - Configure
extension AboutAuthorViewController {

    func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self

        self.tableView.register(UINib(nibName: "AboutAuthorTableViewCell", bundle: nil),
                                forCellReuseIdentifier: "AboutAuthorTableViewCell")
    }
}

// MARK: - UITableViewDelegate
extension AboutAuthorViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
}

// MARK: - UITableViewDataSource
extension AboutAuthorViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutAuthorTableViewCell", for: indexPath)
        
        let aboutAuthorName = titleAboutAuthor
        cell.textLabel?.text = aboutAuthorName
        
        return cell
    }
}
