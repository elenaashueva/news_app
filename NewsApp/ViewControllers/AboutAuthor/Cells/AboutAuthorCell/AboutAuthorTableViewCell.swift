//
//  AboutAuthorTableViewCell.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import UIKit

class AboutAuthorTableViewCell: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameAuthorLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    let authorName = NSLocalizedString("AuthorName", comment: "")
    let descriptionProject = NSLocalizedString("DescriptionProject", comment: "")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        photoImageView.image = UIImage(named: "photo_Elena")
        nameAuthorLabel.text = authorName
        descriptionLabel.text = descriptionProject
        descriptionLabel.numberOfLines = 0
    }
}
