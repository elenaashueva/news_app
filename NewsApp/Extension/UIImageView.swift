//
//  UIImageView.swift
//  NewsApp
//
//  Created by Elena Ashueva
//

import Foundation
import UIKit

extension UIImageView {

    func loadImage(withUrl urlString : String) {
        if let url = URL(string: urlString) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async {
                    self.image = UIImage(data: data)
                }
            }
            task.resume()
        }
    }
}
